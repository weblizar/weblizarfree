<!-- blog section -->
<div class="container weblizar_blog">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="center-title">
				<?php 
				$blog_titl = get_theme_mod('blog_title',__('Latest Blog','weblizar'));
				if (!empty ($blog_titl)) { ?>
					<div class="heading-title">
						<h2 class="h2-section-title weblizar_blog_title"><?php echo esc_html($blog_titl); ?></h2>
					</div>
				<?php } 
				$blog_text = get_theme_mod('blog_text',__('Lorem Ipsum is simply dummy text of the printing and typesetting industry.','weblizar'));
				if (!empty ($blog_text)) { ?>
					<p class="weblizar_blog_text"><?php echo wp_kses_post($blog_text); ?> </p>
				<?php } ?>
				<div class="space-sep20"></div>
			</div>
		</div>
	</div>
	<div class="row masonry1">
		<?php
		
		$args = array( 'post_type' => 'post', 'posts_per_page' =>3,'ignore_sticky_posts' => 1);		
		$post_type_data = new WP_Query( $args );
		$i=1;
		while($post_type_data->have_posts()):
			$post_type_data->the_post();
		    ?>							
			<div class="col-md-4 col-sm-4" id="row-<?php echo esc_attr($i); ?>">
				<div class="feature animated fadeIn animatedVisi" data-animtype="fadeIn" data-animrepeat="0" data-animspeed="1s" data-animdelay="0s" style="-webkit-animation: 1s 0s;">
					<div class="feature-image img-overlay">							
						<?php if(has_post_thumbnail()): ?>
						<?php $default=array('class'=>'img-responsive'); 
							the_post_thumbnail('weblizar_blog_img', $default); ?>
						<?php endif; ?>		
					</div>			
					<div class="feature-content">
						<h3 class="h3-blog-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?>	</a></h3>
						<?php the_excerpt(); ?>
					</div>
					<div class="feature-details">
						<span><i class="fa fa-picture-o"></i></span>					
						<span><i class="fa fa-calendar"></i><?php the_date(); ?></span>
						<span><i class="fa fa-user"></i><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php the_author(); ?></a></span>					
					</div>		
				</div>
			</div>
			<?php if($i%3==0){ echo "<div class='clearfix'></div>"; } $i++; 
		endwhile; ?>		
	</div>
	<?php 
	$read_more = get_theme_mod( 'read_more' );
	if(!empty($read_more)){ ?>
		<ul class="post-footer post-btn1">
			<li>
				<a href="<?php echo esc_html( get_theme_mod( 'read_more' ));  ?>" class="append-button btn btn-color">
					<?php esc_html_e('Show More', 'weblizar'); ?>
				</a>
			</li>
		</ul>
	<?php } ?>
</div>