<?php
// Template Name: Home Page
get_header(); 
if ( weblizar_theme_is_companion_active() ) {
	get_template_part('home','slider1');
	?>
	<div class="content-wrapper">    
		<div class="body-wrapper">
			<?php 
			get_template_part('home','service'); 
			$blog_home = absint(get_theme_mod('blog_home', 1));
			if ( $blog_home == 1) {
				get_template_part('home','blog');
			}
		    ?>			
		</div>
	</div><!--.content-wrapper end -->
<?php } else { 
    get_template_part( 'no', 'content' );
}
get_footer(); ?>