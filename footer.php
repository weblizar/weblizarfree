<?php 
 /* The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage weblizar
 */
?>
<footer>
    <div class="footer">
    	<?php if(is_active_sidebar( 'footer-widget-area' )) { ?>
	        <div class="container">
	            <div class="footer-wrapper">
	                <div class="row">
						<?php  dynamic_sidebar( 'footer-widget-area' );   ?>
					</div>			
	            </div>
	        </div>
	    <?php } ?>
        <div class="copyright">
            <div class="container">
                <div class="row">
					<div class="col-md-6 col-sm-6">
						<?php if ( weblizar_theme_is_companion_active() ) { ?>
	                        <div class="copyright-text weblizar_footer_customizations">
								<?php echo esc_html( get_theme_mod( 'weblizar_footer_customization', __('&copy; Copyright 2020. All Rights Reserved','weblizar') )); ?>
				                <a target="_blank" rel="nofollow" href="<?php echo esc_url( get_theme_mod( 'weblizar_deve_link' ) ); ?>">
				                   <?php echo esc_html( get_theme_mod( 'weblizar_develop_by' )); ?></a>
             				</div>
						<?php  } else { ?>
				            <div class="copyright-text weblizar_footer_customizations">
				                <?php esc_html_e('&copy; Copyright 2020. All Rights Reserved','weblizar'); ?>
				            </div>
				        <?php } ?>    
                    </div>
                    <?php 
					if ( weblizar_theme_is_companion_active() ) {
					$footer_section_social_media_enbled = absint(get_theme_mod( 'footer_section_social_media_enbled', 1 ));
					if ($footer_section_social_media_enbled == 1) { ?>
	                    
						<div class="social-icons">
							<ul> 
								<?php 
								$fb_link = get_theme_mod('fb_link');
								if ( ! empty ( $fb_link ) ) { ?>
								   <li class="facebook" data-toggle="tooltip" data-placement="bottom" title="<?php esc_attr_e('Facebook','weblizar') ?>"><a  href="<?php echo esc_url( get_theme_mod( 'fb_link' ) ); ?>"><i class="fab fa-facebook-f"></i></a></li>
								<?php } 
								$twitter_link = get_theme_mod('twitter_link');
								if ( ! empty ( $twitter_link ) ) { ?>
									<li class="twitter" data-toggle="tooltip" data-placement="bottom" title="<?php esc_attr_e('Twitter','weblizar') ?>"><a href="<?php echo esc_url( get_theme_mod( 'twitter_link' ) ); ?>"><i class="fab fa-twitter"></i></a></li>
								<?php } 
								$linkedin_link = get_theme_mod('linkedin_link');
								if ( ! empty ( $linkedin_link ) ) { ?>					
									<li class="linkedin" data-toggle="tooltip" data-placement="bottom" title="<?php esc_attr_e('Linkedin','weblizar') ?>"><a href="<?php echo esc_url( get_theme_mod( 'linkedin_link' ) ); ?>"><i class="fab fa-linkedin-in"></i></a></li>
								<?php }  ?>
							</ul>
						</div>
		                   
					<?php } } ?>
                </div>
            </div>
        </div>			 
   	</div>
   	 </div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>