<?php
/*	*Theme Name	: Weblizar
	*Theme Core Functions and Codes
*/	


	/* Get the plugin */
	if ( ! function_exists( 'weblizar_theme_is_companion_active' ) ) {
	    function weblizar_theme_is_companion_active() {
	        require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	        if ( is_plugin_active(  'weblizar-companion/weblizar-companion.php' ) ) {
	            return true;
	        } else {
	            return false;
	        }
	    }
	}

	/**Includes reqired resources here**/
	require( get_template_directory() . '/core/menu/weblizar_bootstrap_navwalker.php' ); // for Default Menus
	require( get_template_directory() . '/core/menu/default_menu_walker.php' ); // for Default Menus
	require( get_template_directory() . '/core/comment-box/comment-function.php' ); //for comments
	require( get_template_directory() . '/class-tgm-plugin-activation.php' );
	require(get_template_directory()  . '/core/custom-header.php');	

	add_action( 'after_setup_theme', 'weblizar_setup' ); 	
	function weblizar_setup()
	{	
		add_theme_support( 'title-tag' );
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
	    add_theme_support( 'wc-product-gallery-lightbox' );
	    add_theme_support( 'wc-product-gallery-slider' );
		global $content_width;
		//content width
		if ( ! isset( $content_width ) ) $content_width = 720; //px
	
		/*
	     * Make theme available for translation.
	     * Translations can be filed in the /languages/ directory.
	     * If you're building a theme based on Theme Palace, use a find and replace
	     * to change 'boost-biz' to the name of your theme in all the template files.
	     */
	    load_theme_textdomain( 'weblizar' );	
		
		add_theme_support( 'post-thumbnails' ); //supports featured image
		// This theme uses wp_nav_menu() in one location.
		register_nav_menu( 'primary', esc_html__( 'Primary Menu', 'weblizar' ) );
		add_theme_support( 'customize-selective-refresh-widgets' );

		/* Gutenberg */
		add_theme_support( 'wp-block-styles' );
		add_theme_support( 'align-wide' );

		/* Add editor style. */
		add_theme_support( 'editor-styles' );
		add_theme_support( 'dark-editor-style' );
		
		// Logo
		add_theme_support( 'custom-logo', array(
			'width'       => 250,
			'height'      => 250,
			'flex-width'  => true,
			'flex-height' => true,
			'header-text' => array( 'site-title', 'site-description' ),
		));
		// theme support 	
		$args = array('default-color' => '000000',);
		add_theme_support( 'custom-background', $args); 
		add_theme_support( 'automatic-feed-links');
		add_theme_support( 'html5');
		add_editor_style();
	}
	
	
	function weblizar_scripts()	{	
		
		//** font-awesome-4.7.0 **//
		
		wp_enqueue_style('weblizar-font-awesome-latest', get_template_directory_uri(). '/css/font-awesome-5.11.2/css/all.css');
		wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
		wp_enqueue_style('weblizar-responsive', get_template_directory_uri() . '/css/responsive.css');
		wp_enqueue_style('weblizar-flat-blue', get_template_directory_uri() . '/css/skins/flat-blue.css');	
		wp_enqueue_style('weblizar-theme-menu', get_template_directory_uri() . '/css/theme-menu.css');
		wp_enqueue_style('weblizar-carousel', get_template_directory_uri() . '/css/carousel.css');
		wp_enqueue_style('swiper-css', get_template_directory_uri() . '/css/swiper.css');
		wp_enqueue_style( 'weblizar-style', get_stylesheet_uri() );
		
		// Js
		wp_enqueue_script( 'weblizar-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), true, true );
		wp_enqueue_script('bootstrap', get_template_directory_uri() .'/js/bootstrap.js', array( 'jquery' ), true, true);
		wp_enqueue_script('weblizar-menu', get_template_directory_uri() .'/js/menu.js', array('jquery'), true, true );		
		wp_enqueue_script('swiper', get_template_directory_uri() .'/js/swiper.js', array( 'jquery' ), true, true);	

		
		$slider_image_speed = absint(get_theme_mod('slider_image_speed','2000'));
		$sticky_header = get_theme_mod('sticky_header','on');
		

		$ajax_data = array(
            'image_speed'     => $slider_image_speed,
            'sticky_header'   => $sticky_header,
        );

        wp_enqueue_script( 'weblizar-ajax-front', get_template_directory_uri() . '/js/weblizar-ajax-front.js', array( 'jquery' ), true, true );
        wp_localize_script( 'weblizar-ajax-front', 'ajax_admin', array(
                'ajax_url'    => admin_url( 'admin-ajax.php' ),
                'admin_nonce' => wp_create_nonce( 'admin_ajax_nonce' ),
                'ajax_data'   => $ajax_data,
        ) );
	}
	add_action('wp_enqueue_scripts', 'weblizar_scripts'); 
	if ( is_singular() ) wp_enqueue_script( "comment-reply" ); 

	// Read more tag to formatting in blog page 
	function weblizar_content_more($more)
	{  global $post;							
	   return '<div class="blog-post-details-item blog-read-more"><a href="'.esc_url(get_permalink(),'weblizar').'">'.esc_html("Read More....",'weblizar').'</a></div>';
	}   
	add_filter( 'the_content_more_link', 'weblizar_content_more' );
	
	/*
	* Weblizar widget area
	*/
	add_action( 'widgets_init', 'weblizar_widgets_init');
	function weblizar_widgets_init() {
		/*sidebar*/
		register_sidebar( array(
			'name' 			=> esc_html__( 'Sidebar', 'weblizar' ),
			'id' 			=> 'sidebar-primary',
			'description' 	=> esc_html__( 'The primary widget area', 'weblizar' ),
			'before_widget' => '<div id="%1$s" class="sidebar-block %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="h3-sidebar-title sidebar-title">',
			'after_title'   => '</h3>'
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Footer Widget Area', 'weblizar' ),
			'id'            => 'footer-widget-area',
			'description'   => esc_html__( 'footer widget area', 'weblizar' ),
			'before_widget' => '<div id="%1$s" class="col-md-3 col-sm-3 footer-col %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<div class="footer-title">',
			'after_title'   => '</div>',
		) );             
	}
	
	/*
	* Image resize and crop
	*/
	if ( ( 'add_image_size' ) ) 
	{ 
		add_image_size('weblizar_media_blog_img',800,400,true);
		add_image_size('weblizar_blog_img',350,150,true);
	}

	add_action( 'tgmpa_register', 'weblizar_parallax_plugin_recommend' );
	function weblizar_parallax_plugin_recommend() {
		$plugins = array(
			array(
				'name'     => esc_html__('Weblizar Companion','weblizar'),
				'slug'     => 'weblizar-companion',
				'required' => false,
			),
		);
		tgmpa( $plugins );
	}

// Replaces the excerpt "more" text by a link
function weblizar_excerpt_more($more)
{
	if ( is_admin() ) {
	    $more = '...';
	    return $more;
	}
}

add_filter('excerpt_more', 'weblizar_excerpt_more');


function weblizar_add_editor_styles() {
    $font_url = str_replace( ',', '%2C', '//fonts.googleapis.com/css?family=Lato:300,400,700' );
    add_editor_style( $font_url );
}
add_action( 'after_setup_theme', 'weblizar_add_editor_styles' );

add_action( 'wp_enqueue_scripts', 'weblizar_custom_css' );
function weblizar_custom_css() {
    $output = '';
	$output .= 'ul.post-footer {
				text-align: center;
				list-style: none;
				margin-top: 50px;
			}';
	$output .= '.item {
			  margin-bottom: 30px;
			}';
	$output .= 'a.append-button.btn.btn-color{
				background-color: #3498db;
				border: 1px solid transparent;
				color: #fff;
				font-size: 21px;
				border-radius: 6px;
				line-height: 1.4;
			}';
	$output .= 'a.append-button.btn.btn-color:hover {
			  opacity: 0.9;
			  color: #fff;
			}';

	$output .= '.logo a img {
			  height: '. esc_attr(get_theme_mod('logo_height','55')).'px;
			  width: '.esc_attr(get_theme_mod('logo_width','150')).'px;
			}';

	//custom css
	$custom_css = get_theme_mod('custom_css') ; 
	if (!empty ($custom_css)) {
        $output .= get_theme_mod('custom_css') . "\n";
    }

    wp_register_style( 'weblizar-custom-header-style', false );
    wp_enqueue_style( 'weblizar-custom-header-style', get_template_directory_uri() . '/css/custom-header-style.css' );
    wp_add_inline_style('weblizar-custom-header-style', $output );
}